import React, {useState} from 'react';
import { Step, Stepper, StepLabel, Button } from '@material-ui/core';
import './App.css';

function App() {

  const [activeStep, setActiveStep] = useState(0)
  const steps = ['Step 1', 'Step 2', 'Step 3', 'Step 4', 'Step 5']
  const handleNext = () => {
    setActiveStep( (prev) => prev + 1 )
  }
  const handleBack = () => {
    setActiveStep( (prev) => prev - 1 )
  }

  return (
    <>
      <Stepper 
        activeStep={activeStep}
        alternativeLabel={true}
      >
        {steps.map( (step) => {
          return (
            <Step key={step}>
              <StepLabel>{step}</StepLabel>
            </Step>
          )
        })}
      </Stepper>
    
    { activeStep > 0 && (
      <Button 
      variant='contained'
      color='primary'
      onClick={handleBack}
      >
        Back
      </Button>
    )}
    { activeStep === steps.length - 1 ? (
      <Button 
      variant='contained'
      color='primary'
      >
        Finish
      </Button>
    ) 
    : (
      <Button 
      variant='contained'
      color='primary'
      onClick={handleNext}
      >
        Next
      </Button>
    )}
  </>
  );
}

export default App;
